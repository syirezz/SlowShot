from time import sleep
import pygame
import sys
import random

# Constants for game window
WINDOW_WIDTH, WINDOW_HEIGHT = 800, 600
FPS = 60

# Initialize Pygame
pygame.init()

# Set up display
WINDOW = pygame.display.set_mode((WINDOW_WIDTH, WINDOW_HEIGHT))
pygame.display.set_caption('SlowShot |Cheat Code Feature') 

# Set up the colors
BLACK = (0, 0, 0)
RED = (255, 0, 0)
GREEN = (0, 255, 0)
WHITE = (255, 255, 255)

# Set up the game clock
clock = pygame.time.Clock()

# Player properties
player_rect = pygame.Rect(WINDOW_WIDTH//2, WINDOW_HEIGHT//2, 50, 50)
player_speed = 9
player_ammo = 20
player_gun_length = 30
player_gun_reload_time = 1  # Time in seconds
# Shooting effect properties
shoot_effect_duration = 100  # in milliseconds
shoot_effect_color = (255, 255, 0)  # yellow
recoil_distance = 50
# Shooting effect variables
show_shoot_effect = False
shoot_effect_start_time = 0
shoot_effect_end_time = 0
cheat_code = ""

game_over_font = pygame.font.Font(None, 50)
game_over_color = (255, 0, 0)
game_over_text_position = (WINDOW_WIDTH // 2 - 100, WINDOW_HEIGHT // 2)


# Ammo text properties
ammo_font = pygame.font.Font(None, 36)
ammo_color = WHITE
ammo_text_position = (10, 10)


# Enemy properties
enemy_rect = pygame.Rect(100, 100, 50, 50)
enemy_speed = 2

# Bullet properties
bullet_radius = 5
bullets = []

# Time tracking for gun reload
last_gun_fire_time = 0
time_frozen = False
time_frozen_start_time = 0
time_frozen_end_time = 0
time_freeze_duration = 100000000000

# Function to draw gun
def draw_gun(surface, color, start_pos, end_pos):
    pygame.draw.line(surface, color, start_pos, end_pos, 5)

# Function to draw enemies
def draw_enemies(surface, color, rect):
    pygame.draw.rect(surface, color, rect)

# Function to spawn a new enemy
def spawn_enemy():
    x = random.randint(0, WINDOW_WIDTH - 50)
    y = random.randint(0, WINDOW_HEIGHT - 50)
    return pygame.Rect(x, y, 50, 50)

# Main game loop
running = True
while running:
    # Get the current time
    current_time = pygame.time.get_ticks()

    # Handle events
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            running = False
        elif event.type == pygame.MOUSEBUTTONDOWN and player_ammo > 0:
            if event.button == 1 and current_time - last_gun_fire_time > player_gun_reload_time * 1000:
                # Gun fire
                last_gun_fire_time = current_time
                player_ammo -= 1
                mouse_x, mouse_y = pygame.mouse.get_pos()
                bullets.append(((player_rect.centerx, player_rect.centery), (mouse_x, mouse_y)))

                # Trigger shooting effect
                show_shoot_effect = True
                shoot_effect_start_time = current_time
                shoot_effect_end_time = current_time + shoot_effect_duration

                # Check -x or x
                if mouse_x < player_rect.centerx:
                    player_rect.x += recoil_distance
                else:
                    player_rect.x -= recoil_distance
        elif event.type == pygame.KEYDOWN:
            cheat_code += event.unicode  # Append the pressed key to the cheat code string
            cheat_code = cheat_code[-4:]  # Keep only the last 4 characters of the cheat code
            # print(cheat_code)
            if cheat_code.lower() == "vc":  # Check if the cheat code is entered
                player_ammo = 200
                player_gun_reload_time = 0.001
        elif event.type == pygame.KEYUP:
            cheat_code = ""  # Reset the cheat code when a key is released


    # Get keys pressed
    keys = pygame.key.get_pressed()
    is_player_moving = keys[pygame.K_a] or keys[pygame.K_d] or keys[pygame.K_w] or keys[pygame.K_s]
    player_rect.x += (keys[pygame.K_d] - keys[pygame.K_a]) * player_speed
    player_rect.y += (keys[pygame.K_s] - keys[pygame.K_w]) * player_speed
    player_rect.clamp_ip(WINDOW.get_rect())  # Keep player within the screen

    if keys[pygame.K_r]:  # Reload gun on 'R' key
        player_ammo = 20
        last_gun_fire_time = current_time


    # Freeze time if the player is not moving
    if not is_player_moving and not time_frozen:
        time_frozen = True
        time_frozen_start_time = current_time
        time_frozen_end_time = current_time + time_freeze_duration

    # Unfreeze time if the player starts moving
    elif is_player_moving and time_frozen:
        time_frozen = False
        time_frozen_end_time = current_time

    if time_frozen and current_time < time_frozen_end_time:
        # Draw some frozen time effect, for example, a message
        font = pygame.font.Font(None, 36)
        text = font.render('Time Frozen!', True, WHITE)
        WINDOW.blit(text, (WINDOW_WIDTH // 2 - 100, WINDOW_HEIGHT // 2))

    # Enemy movement


    enemy_rect.x += enemy_speed if player_rect.x > enemy_rect.x else -enemy_speed
    enemy_rect.y += enemy_speed if player_rect.y > enemy_rect.y else -enemy_speed
    enemy_rect.clamp_ip(WINDOW.get_rect())  # Keep enemy within the screen

    if player_rect.x > enemy_rect.x:
        enemy_rect.x += enemy_speed
    elif player_rect.x < enemy_rect.x:
        # Only move the enemy along the x-axis
        enemy_rect.x -= enemy_speed



    # Bullet collision with enemy
    for bullet in bullets:
    # Check x-coordinate collision only when player is moving to the left
        if player_rect.x < enemy_rect.x and enemy_rect.collidepoint(bullet[1][0], enemy_rect.centery):
            bullets.remove(bullet)
            enemy_rect = spawn_enemy()

    # Draw everything
    WINDOW.fill(BLACK)  # Clear screen with black background
    if show_shoot_effect and current_time < shoot_effect_end_time:
        draw_gun(WINDOW, shoot_effect_color, player_rect.center, pygame.mouse.get_pos())

    pygame.draw.rect(WINDOW, RED, player_rect)  # Draw the player
    draw_enemies(WINDOW, GREEN, enemy_rect)  # Draw the enemy

    # Draw bullets
    for bullet in bullets:
        pygame.draw.circle(WINDOW, WHITE, bullet[1], bullet_radius)
    ammo_text = ammo_font.render(f'Ammo: {player_ammo}', True, ammo_color)
    WINDOW.blit(ammo_text, ammo_text_position)

    #Enemy kill player and restart game on space

    if player_ammo < 5:
        ammo_text_2 = ammo_font.render("Press 'R' to reload!", True, RED)
        WINDOW.blit(ammo_text_2, (10, 40))

    # Flip the display
    pygame.display.flip()

    # Cap the frame rate
    clock.tick(FPS)

    if current_time >= shoot_effect_end_time:
        show_shoot_effect = False

    # if player_rect.colliderect(enemy_rect):
    #     running = False

    # if not running:
    #     game_over_text = game_over_font.render('Game Over!', True, game_over_color)
    #     WINDOW.blit(game_over_text, game_over_text_position)

    #     # Flip the display
    #     pygame.display.flip()

    #     # Introduce a delay (4 seconds)
    #     pygame.time.delay(3000)
# sleep(4)
# Quit the game
pygame.quit()
sys.exit()


